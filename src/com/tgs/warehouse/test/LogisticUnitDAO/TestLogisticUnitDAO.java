package com.tgs.warehouse.test.LogisticUnitDAO;

import com.tgs.warehouse.main.connection.DBConnectionImpl;
import com.tgs.warehouse.main.dao.LogisticUnitDAOImpl;
import com.tgs.warehouse.main.entities.ProductPackage;
import com.tgs.warehouse.main.entities.ProductPallet;
import org.junit.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;


public class TestLogisticUnitDAO {

    private static LogisticUnitDAOImpl logUnitDao;
    private static Connection connection;
    private static DBConnectionImpl conn = new DBConnectionImpl();
    private long startTime;

    private ProductPallet createTestObject() {
        LinkedList<ProductPackage> linkedList = new LinkedList();
        linkedList.add(new ProductPackage("roz", "hard"));
        linkedList.add(new ProductPackage("verde", "soft"));
        return new ProductPallet("kekistanni", linkedList);
    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        connection = conn.openConnection().get();
        logUnitDao = new LogisticUnitDAOImpl();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        logUnitDao = null;
        conn.closeConnection(connection);
    }

    @Before
    public void setUp() throws Exception {
        startTime = System.nanoTime();
    }

    @After
    public void tearDown() throws Exception {
        long estimatedTime = System.nanoTime() - startTime;
        System.out.println(estimatedTime + " nanoseconds");
    }


    @Test
    public void testInsertProductPallet() {

        ProductPallet pp = createTestObject();

        try {
            pp = logUnitDao.insertProductPallet(pp);

            assertNotNull(pp.getId());

            ProductPallet ppFromDb = logUnitDao.getProductPalletById(pp.getId());
            assertEquals(ppFromDb, pp);

        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testDeleteInvalidProductPallet() {

        try {
            Long invalidId = (long) -1;
            boolean result = logUnitDao.deleteProductPallet(invalidId);

            assertFalse(result);

        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testDeleteValidProductPallet() {

        try {
            ProductPallet pp = createTestObject();
            Long validId = logUnitDao.insertProductPallet(pp).getId();
            boolean result = logUnitDao.deleteProductPallet(validId);

            assertTrue(result);
            ProductPallet ppFromDb = logUnitDao.getProductPalletById(validId);
            assertNull(ppFromDb);


        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSearchProductPallet() {

        ProductPallet pp = createTestObject();
        pp.setDescription("testSearchProductPallet");

        try {
            deleteEntryMadeBySearch();
            pp = logUnitDao.insertProductPallet(pp);
            List<ProductPallet> ppTestList = new ArrayList<>();
            ppTestList.add(pp);
            List<ProductPallet> ppFromDb = logUnitDao.search("testSearchProductPallet");

            assertNotNull(ppFromDb);
            assertArrayEquals(ppTestList.toArray(), ppFromDb.toArray());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    private void deleteEntryMadeBySearch() {
        List<ProductPallet> toBeDeletedList = logUnitDao.search("testSearchProductPallet");
        for (int i = 0; i < toBeDeletedList.size(); i++) {
            logUnitDao.deleteProductPallet(toBeDeletedList.get(i).getId());
        }
    }

}
