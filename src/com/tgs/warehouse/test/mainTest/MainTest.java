package com.tgs.warehouse.test.mainTest;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class MainTest {
    public static void main(String[] args) {


        try (OutputStream output = new FileOutputStream("src/com/tgs/warehouse/main/resources/properties.properties")) {

            Properties prop = new Properties();

            // set the properties value
            prop.setProperty("db.url.1", "warehouse");
            prop.setProperty("db.user.1", "postgres");
            prop.setProperty("db.password.1", "1234");

            // save properties to project root folder
            prop.store(output, null);

            System.out.println(prop);

        } catch (IOException io) {
            io.printStackTrace();
        }
    }
}