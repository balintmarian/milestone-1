package com.tgs.warehouse.main.entities;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class ProductPallet {
    private long id;
    private String description;
    private LinkedList<ProductPackage> packages;

    public ProductPallet(long id, String description, List<ProductPackage> packages) {
        this.id = id;
        this.description = description;
        this.packages = (LinkedList<ProductPackage>) packages;

    }

    public ProductPallet(String description, List<ProductPackage> packages) {
        this.packages = (LinkedList<ProductPackage>) packages;
        this.description = description;
    }

    public ProductPallet(long id, String description) {
        this.id = id;
        this.description = description;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description.toLowerCase();
    }

    public LinkedList<ProductPackage> getPackages() {
        return packages;
    }

    public void setPackages(LinkedList<ProductPackage> packages) {
        this.packages = packages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductPallet that = (ProductPallet) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
