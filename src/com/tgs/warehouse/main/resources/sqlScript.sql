-- Database: warehouse

-- DROP DATABASE warehouse;

--CREATE DATABASE warehouse
--    WITH 
--    OWNER = postgres
--    ENCODING = 'UTF8'
--    LC_COLLATE = 'English_United States.1252'
--    LC_CTYPE = 'English_United States.1252'
 --   TABLESPACE = pg_default
 --   CONNECTION LIMIT = -1;
CREATE TABLE product_pallet(
	id serial PRIMARY KEY,
	description VARCHAR(100)
);

CREATE TABLE product_package(
	id serial PRIMARY KEY,
	description VARCHAR(100),
	type VARCHAR(50)
);



COMMENT ON DATABASE warehouse
    IS 'data base for technical training';