package com.tgs.warehouse.main.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;
import java.util.logging.Logger;

public class DBConnectionImpl implements DBConnection {


    public Optional<Connection> openConnection() {
        Connection connection = null;

        try {

            Class.forName("org.postgresql.Driver");

            connection = DriverManager
                    .getConnection(
                            "jdbc:postgresql://localhost:5432/warehouse",
                            "postgres", "1234");

            System.out.println("Opened database successfully");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return Optional.ofNullable(connection);
    }

    public void closeConnection(Connection connection) {
        try {
            if (connection != null) {
                connection.close();
                System.out.println("Connection closed successfully");
            } else {
                System.out.println("no available connection to close");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("something went wrong in closing connection");
        }
    }
}
