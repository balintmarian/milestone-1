package com.tgs.warehouse.main.connection;

import java.sql.Connection;
import java.util.Optional;

public interface DBConnection {

     Optional<Connection> openConnection();

    void closeConnection(Connection connection);
}