package com.tgs.warehouse.main;
import com.tgs.warehouse.test.LogisticUnitDAO.TestLogisticUnitDAO;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;


public class Main {

    public static void main(String[] args) {
        System.out.println("|Starting LogisticUnitDAOImpl test|");
        JUnitCore junit = new JUnitCore();
        Result result = junit.run();
    }
}

