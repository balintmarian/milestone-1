package com.tgs.warehouse.main.dao;

import com.tgs.warehouse.main.entities.ProductPallet;

import java.util.List;

public interface LogisticUnitDAO {

    ProductPallet insertProductPallet(ProductPallet newProduct);

    boolean deleteProductPallet(Long id);

    ProductPallet getProductPalletById(Long id);

    List search(String description);

}
