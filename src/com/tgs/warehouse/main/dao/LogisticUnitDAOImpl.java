package com.tgs.warehouse.main.dao;

import com.tgs.warehouse.main.connection.DBConnectionImpl;
import com.tgs.warehouse.main.entities.ProductPackage;
import com.tgs.warehouse.main.entities.ProductPallet;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class LogisticUnitDAOImpl implements LogisticUnitDAO {
    private DBConnectionImpl dbConnection = new DBConnectionImpl();

    @Override
    public ProductPallet insertProductPallet(ProductPallet newProduct) {
        PreparedStatement preparedStatementInsert;
        String insertProductPalletSQL = "INSERT INTO product_pallet (description) VALUES(?) RETURNING id";
        Optional<Connection> optionalConnection = dbConnection.openConnection();
        if (!optionalConnection.isPresent()) {
            throw new RuntimeException("Needs a connection");
        }

        final Connection connection = optionalConnection.get();
        try {
            connection.setAutoCommit(false);

            preparedStatementInsert = connection.prepareStatement(insertProductPalletSQL);
            preparedStatementInsert.setString(1, newProduct.getDescription());
            ResultSet rsKey = preparedStatementInsert.executeQuery();


            if (rsKey.next()) {
                newProduct.setId(rsKey.getLong(1));
            }
            insertProductPackage(newProduct);
            connection.commit();


            return newProduct;


        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            dbConnection.closeConnection(connection);
        }

        return null;
    }

    private void insertProductPackage(ProductPallet productPallet) {

        PreparedStatement preparedStatementInsert;
        String insertProductPackageSQL = "INSERT INTO product_package (description,type,product_pallet_id) VALUES(?,?,?)";
        Optional<Connection> optionalConnection = dbConnection.openConnection();
        if (!optionalConnection.isPresent()) {
            throw new RuntimeException("Needs a connection");
        }

        final Connection connection = optionalConnection.get();
        try {

            preparedStatementInsert = connection.prepareStatement(insertProductPackageSQL);

            for (ProductPackage pP : productPallet.getPackages()) {
                preparedStatementInsert.setString(1, pP.getDescription());
                preparedStatementInsert.setString(2, pP.getType());
                preparedStatementInsert.setLong(3, productPallet.getId());
                preparedStatementInsert.executeUpdate();
            }


        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public boolean deleteProductPallet(Long id) {
        PreparedStatement preparedStatementDelete;
        String deleteProductPalletSQL = "DELETE FROM product_pallet WHERE id = ? ";
        String deleteProductPackageSQL = "DELETE FROM product_package WHERE product_pallet_id = ? ";

        int isWorking = -1;
        Optional<Connection> optionalConnection = dbConnection.openConnection();
        if (!optionalConnection.isPresent()) {
            throw new RuntimeException("Needs a connection");
        }

        final Connection connection = optionalConnection.get();
        try {
            connection.setAutoCommit(false);

            preparedStatementDelete = connection.prepareStatement(deleteProductPackageSQL);
            preparedStatementDelete.setLong(1, id);
            preparedStatementDelete.executeUpdate();
            preparedStatementDelete = connection.prepareStatement(deleteProductPalletSQL);
            preparedStatementDelete.setLong(1, id);
            isWorking = preparedStatementDelete.executeUpdate();

            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            dbConnection.closeConnection(connection);
        }
        return (isWorking > 0);
    }


    @Override
    public ProductPallet getProductPalletById(Long id) {
        PreparedStatement preparedStatementGetById;
        String getByIdProductPalletSQL = "SELECT * FROM product_pallet WHERE id = ?";
        Optional<Connection> optionalConnection = dbConnection.openConnection();
        if (!optionalConnection.isPresent()) {
            throw new RuntimeException("Needs a connection");
        }

        final Connection connection = optionalConnection.get();
        try {
            List<ProductPackage> productPackages = getProductPackagesByPalletId(id);

            preparedStatementGetById = connection.prepareStatement(getByIdProductPalletSQL);
            preparedStatementGetById.setLong(1, id);
            ResultSet resultSetPallet = preparedStatementGetById.executeQuery();
            if (resultSetPallet.next()) {
                return new ProductPallet(
                        resultSetPallet.getLong("id"),
                        resultSetPallet.getString("description"), productPackages);

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            dbConnection.closeConnection(connection);
        }
        return null;
    }

    private List<ProductPackage> getProductPackagesByPalletId(long id) {
        List<ProductPackage> productPackages = new LinkedList<>();
        PreparedStatement preparedStatementGetPackage;
        String getByIdProductPackageSQL = "SELECT * FROM product_package WHERE product_pallet_id = ?";
        Optional<Connection> optionalConnection = dbConnection.openConnection();
        if (!optionalConnection.isPresent()) {
            throw new RuntimeException("Needs a connection");
        }

        final Connection connection = optionalConnection.get();
        try {
            preparedStatementGetPackage = connection.prepareStatement(getByIdProductPackageSQL);
            preparedStatementGetPackage.setLong(1, id);
            ResultSet resultSetPackage = preparedStatementGetPackage.executeQuery();

            if (resultSetPackage.next()) {
                productPackages.add(new ProductPackage(
                        resultSetPackage.getString("description"),
                        resultSetPackage.getString("type")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productPackages;
    }

    @Override
    public List search(String description) {
        description = description.toLowerCase();
        String searchPalletSQL = "select product_pallet.id, product_pallet.description from product_pallet " +
                "inner join product_package on product_pallet.id =product_package.product_pallet_id " +
                "WHERE product_package.description LIKE ? OR  product_pallet.description LIKE ? GROUP BY product_pallet.id";

        Optional<Connection> optionalConnection = dbConnection.openConnection();
        if (!optionalConnection.isPresent()) {
            throw new RuntimeException("Needs a connection");
        }

        final Connection connection = optionalConnection.get();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(searchPalletSQL);
            preparedStatement.setString(1, "%" + description + "%");
            preparedStatement.setString(2, "%" + description + "%");

            List<ProductPallet> productPalletList = new LinkedList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                productPalletList.add(new ProductPallet(
                        resultSet.getLong("id"), resultSet.getString("description")));
            }
            return productPalletList;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbConnection.closeConnection(connection);
        }

        return null;
    }
}
